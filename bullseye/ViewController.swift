//
//  ViewController.swift
//  bullseye
//
//  Created by Ashlee on 12/09/2019.
//  Copyright © 2019 Green Farm Games Ltd. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var target: UILabel!
    @IBOutlet weak var gameSlider: UISlider!
    @IBOutlet weak var totalRounds: UILabel!
    @IBOutlet weak var totalScore: UILabel!
    // If you don't define with a default value, compiler complains about not having a constructor.
    var currentSliderValue = 0
    var targetGoal = 50
    var currentRound = 0
    var currentScore = 0
    let maxScore = 100
    override func viewDidLoad() {
        super.viewDidLoad()
        // Decorate slider, UISlider only configurable in code
        let thumbImage = UIImage(named: "SliderThumb-Normal")
        let thumbHighLightedImage = UIImage(named: "SliderThumb-Highlighted")
        gameSlider.setThumbImage(thumbImage, for: .normal)
        gameSlider.setThumbImage(thumbHighLightedImage, for: .highlighted)
        
        //Setup min images
        let insets = UIEdgeInsets(top: 0, left: 14, bottom: 0, right: 14)
        let trackLeftImage = UIImage(named: "SliderTrackLeft")!
        let trackLeftResizable =
            trackLeftImage.resizableImage(withCapInsets: insets)
        gameSlider.setMinimumTrackImage(trackLeftResizable, for: .normal)
        
        //Setup max images
        let trackRightImage = UIImage(named: "SliderTrackRight")!
        let trackRightResizable =
            trackRightImage.resizableImage(withCapInsets: insets)
        gameSlider.setMaximumTrackImage(trackRightResizable, for: .normal)
        
        // Do any additional setup after loading the view, typically from a nib.
        startNewGame()
    }

    fileprivate func calculateScore() {
        // add up scoring
        let scoreDifference = abs(currentSliderValue - targetGoal)
        let points: Int
        //let points = maxScore / scoreDifference
        //currentScore += (targetGoal == currentSliderValue) ? maxScore*2 : points * 1
        switch scoreDifference {
        case 0:
            points = maxScore*2
        case 1:
            points = (maxScore / scoreDifference) + (maxScore / 2)
        default:
            points = maxScore / scoreDifference
        }
        currentScore += points
        startNewRound()
    }
    
    @IBAction func showAlert() {
        let scoreDifference = abs(currentSliderValue - targetGoal)
        let message = "The value of the slide is \(currentSliderValue)" +
        "\nThe target value is: \(targetGoal)" +
        "\nThe difference is: \(scoreDifference)"
        let alertTitle = generateAchievementTitle(difference: scoreDifference)
        let alert = UIAlertController(
            title: alertTitle,
            message: message,
            preferredStyle: .alert
        )
        // Notice the difference in callback on swift, the '_ in' is required
        //before you call the function you want as a callback.
        let action = UIAlertAction(title: "FinalScore",
                                   style: .default,
                                   handler: { _ in
                                    self.calculateScore()
                                    }
        )
        alert.addAction(action)
        present(alert,
                animated: true,
                completion:nil
        )
    }
    @IBAction func sliderMove(_slider: UISlider) {
        currentSliderValue = lroundf(_slider.value)
    }
     fileprivate func startNewRound(){
        currentRound += 1
        // Random Value closed range... being inclusive of 1 and 100
        targetGoal = Int.random(in: 1...100)
        currentSliderValue = Int(Float(maxScore / 2).rounded(.down))
        gameSlider.value = Float(currentSliderValue)
        updateLabels()
    }
    fileprivate func updateLabels(){
        target.text = String(describing:targetGoal)
        // Clear round count
        totalRounds.text = String(describing:currentRound)
        totalScore.text =  String(currentScore)
    }
    @IBAction func startNewGame() {
        currentRound = 0
        currentScore = 0
        startNewRound()
        // Transition animation
        let transition = CATransition()
        transition.type = CATransitionType.fade
        // in seconds
        transition.duration = 1
        transition.timingFunction = CAMediaTimingFunction(name:
            CAMediaTimingFunctionName.easeOut)
        view.layer.add(transition, forKey: nil)
        
    }
    fileprivate func generateAchievementTitle(difference: Int) -> String  {
        switch difference {
        case 0:
            return "Perfect!"
        case 1...3:
            return "Horse shoes and Hand grenades!"
        case 3...6:
            return "You Almost had it!"
        case 6...10:
            return "Pretty good!"
        case 10..<20:
            return "Better luck next time"
        default:
            return "Not even close..."
        }
    }
}

