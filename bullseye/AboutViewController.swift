//
//  AboutViewController.swift
//  bullseye
//
//  Created by Ashlee on 13/09/2019.
//  Copyright © 2019 Green Farm Games Ltd. All rights reserved.
//

import UIKit
import WebKit

class AboutViewController: UIViewController {

    @IBOutlet weak var webkitView: WKWebView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Bind the bundle html file to webkitView
        if let url = Bundle.main.url(forResource: "BullsEye",
                                     withExtension: "html") {
            let request = URLRequest(url: url)
            webkitView.load(request)
        }
    }
    
    @IBAction func closeAbout() {
        dismiss(animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
